{% docs wk_map_usage_ping_subscription_id %}

Mapping table used to link a usage ping (dim_usage_ping_id) to a zuora subscription (dim_subscription_id)

{% enddocs %}
